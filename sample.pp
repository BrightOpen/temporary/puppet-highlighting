# Some comments
class abc::vhost {

  package { 'abc-kvm':
    ensure  => $abc::vhost_version,
    require => Repos::File['abc'],
  }

  ssh_authorized_key { "abc@abc_${abc::dc}_${abc::env}":
    ensure         => present,
    user           => 'abc',
    type           => 'ssh-rsa',
    key            => $abc::vhost_abc_pubkey,
    require        => Package['abc-kvm'],
  }

  file { '/data/abc/disk/':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0666',
    require => Package['abc-kvm'],
  }

  file { '/var/abc/datastores':
    ensure  => link,
    target  => '/data/abc/disk/',
    force   => true,
    require => [
      File['/data/abc/disk/'],
    ],
  }
}
